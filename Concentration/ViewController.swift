//
//  ViewController.swift
//  Concentration
//
//  Created by Егор Тупчиенко on 4/8/19.
//  Copyright © 2019 Егор Тупчиенко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    lazy var game = newConcentration()


    @IBOutlet weak var flipCountLabel: UILabel!
    
    @IBOutlet var cardButtons: [UIButton]!
    

    
    @IBAction func newGameBar(_ sender: UIBarButtonItem) {
        print("some")
    }
    @IBAction func touchCard(_ sender: UIButton) {

        if let cardNumber = cardButtons.firstIndex(of: sender) {
            game.choosenCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("chosen card was not in cardButtons")
        }
        
    }


    @IBAction func newGame(_ sender: UIButton) {
        game = newConcentration()
        updateViewFromModel()
        emojiChoices = newEmojiChoices()
    }

    
    


    func newConcentration() -> Concentration {
        return Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
    }
    

    func updateViewFromModel() {
            for index in cardButtons.indices {
                let button = cardButtons[index]
                let card = game.cards[index]
                if card.isFaceUp {
                    button.setTitle(emoji(for: card), for: UIControl.State.normal)
                    button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                } else {
                    button.setTitle("", for: UIControl.State.normal)
                    button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
                }
            }
        flipCountLabel.text = "Flips: \(game.flipCount)"
        }

    

    


    let themes  = [
            ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼", "🐨"],  //        "animals":
            ["🎅", "🤶", "🦌", "☃️", "🎄", "🎁", "🧦", "🔔", "🕯️"],  //        "christmas":
            ["🤮", "🙄", "🤯", "🤩", "🤪", "🤨", "🤠", "🤬", "🤤"],  //        "faces":
            ["🍕", "🍔", "🍣", "🌮", "🍿", "🍗", "🥐", "🥑", "🌽"],  //        "food" :
            ["🦇", "😱", "🙀", "😈", "🎃", "👻", "🍭", "🍬", "🍎"],  //        "halloween" :
            ["⚽️", "🏀", "🏈", "⚾️", "🎾", "🏐", "🏉", "🎱", "🏓"],  //        "sports" :
]
    lazy  var emojiChoices : [String] = newEmojiChoices()
    func newEmojiChoices() -> [String] {
        return themes[Int(arc4random_uniform(UInt32(themes.count)))]
    }
    
    
    
    
    
    var emoji = [Int:String]()
    
    func emoji(for card: Card) -> String {
        if emoji[card.identifier] == nil, emojiChoices.count > 0 {
            let randomIndex = Int(arc4random_uniform(UInt32(emojiChoices.count)))
            emoji[card.identifier] = emojiChoices.remove(at: randomIndex)
        }
        return emoji[card.identifier] ?? "?"
    }
}



