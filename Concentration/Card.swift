//
//  Card.swift
//  Concentration
//
//  Created by Егор Тупчиенко on 5/7/19.
//  Copyright © 2019 Егор Тупчиенко. All rights reserved.
//

import Foundation

struct Card
{
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    static var identifierFactory = 0
    static func getUniqueIdentifier () -> Int {
        Card.identifierFactory += 1
        return Card.identifierFactory
    }
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
    
}



