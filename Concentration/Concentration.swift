//
//  Concetration.swift
//  Concentration
//
//  Created by Егор Тупчиенко on 5/7/19.
//  Copyright © 2019 Егор Тупчиенко. All rights reserved.
//

import Foundation

class Concentration
{
    var cards = [Card]()
    var flipCount = 0
    var indexOfOneOnlyFaceUpCard: Int?
    
    func choosenCard(at index: Int) {
        flipCount += 1
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneOnlyFaceUpCard, matchIndex != index {
                // check if cards match
                if cards[matchIndex].identifier == cards[index].identifier {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                }
                cards[index].isFaceUp = true
                indexOfOneOnlyFaceUpCard  = nil
            } else {
                //either no cards or 2 cards are face up
                for flipDownIndex in cards.indices {
                  cards[flipDownIndex].isFaceUp = false
                }
                cards[index].isFaceUp = true
                indexOfOneOnlyFaceUpCard   = index
            }
        }
        
    }
    
    init(numberOfPairsOfCards: Int){
        for _ in 1...numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        cards.shuffle()
    }
}
