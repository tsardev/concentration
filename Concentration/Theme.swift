//
//  Theme.swift
//  Concentration
//
//  Created by Егор Тупчиенко on 5/10/19.
//  Copyright © 2019 Егор Тупчиенко. All rights reserved.
//

import Foundation

struct ThemeList {
    
    var themeBackground = false
    var identifier : Int
    static var identifierFactory = 0
    static func getUniqueIdentifier () -> Int {
        ThemeList.identifierFactory += 1
        return ThemeList.identifierFactory
    }
    
    init() {
        self.identifier = ThemeList.getUniqueIdentifier()
    }

}




////class ThemeList  {
//private let emojiThemes = [
//    ["🐶", "🐱", "🐭", "🐹", "🐰", "🦊", "🐻", "🐼", "🐨"], // animals
//    ["🎅", "🤶", "🦌", "☃️", "🎄", "🎁", "🧦", "🔔", "🕯️"], // christmas
//    ["🤮", "🙄", "🤯", "🤩", "🤪", "🤨", "🤠", "🤬", "🤤"], // faces
//    ["🍕", "🍔", "🍣", "🌮", "🍿", "🍗", "🥐", "🥑", "🌽"], // food
//    ["🦇", "😱", "🙀", "😈", "🎃", "👻", "🍭", "🍬", "🍎"], // halloween
//    ["⚽️", "🏀", "🏈", "⚾️", "🎾", "🏐", "🏉", "🎱", "🏓"], // sports
////
////}

